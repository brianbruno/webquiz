'use strict';

module.exports = function(app) {
    const usuario_controller = require('../controllers/usuario_controller');

    app.route('/usuario')
        .post(usuario_controller.cadastrar_usuario);

    app.route('/usuario/id/:idUser')
        .get(usuario_controller.verificar_usuario)
};
