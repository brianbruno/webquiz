'use strict';

module.exports = function(app) {
    const sala_controller = require('../controllers/sala_controller');

    app.route('/sala')
        .get(sala_controller.listar_todas_salas)
        .post(sala_controller.cadastrar_sala);

    app.route('/sala/user/:idUser')
        .get(sala_controller.sala_usuario);

    app.route('/sala/id/:idSala')
        .get(sala_controller.buscar_sala);

    app.route('/sala/:idSala/user/:idUser/entrar')
        .post(sala_controller.entrar_em_sala);

    app.route('/sala/:idSala/componente')
        .post(sala_controller.alterar_componente_sala);

    app.route('/sala/:idSala/iniciarapresentacao')
        .post(sala_controller.iniciar_apresentacao);

    app.route('/sala/:idSala/finalizarapresentacao')
        .post(sala_controller.finalizar_apresentacao);

    app.route('/sala/:idSala/selecionarcomponente/:indiceComponente')
        .post(sala_controller.selecionar_componente_apresentacao);

    app.route('/sala/:idSala/componente/:indiceComponente/item/:item')
        .post(sala_controller.votar_componente);
};
