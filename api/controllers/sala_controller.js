'use strict';

const mongoose = require('mongoose');
const Sala = mongoose.model('Sala');
var amqp = require('amqplib/callback_api');

let amqpChannel = null;

amqp.connect('amqp://34.82.122.214:5672', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        amqpChannel = channel;
    });
});
const options = {
    persistent: true,
    noAck: false,
    timestamp: Date.now(),
    contentEncoding: "utf-8",
    contentType: "text/plain"
};

exports.cadastrar_sala = function(req, res) {
    const cadastrar_sala = new Sala(req.body);

    cadastrar_sala.save(function(err, sala) {

        amqpChannel.assertExchange('cadastrar_sala', 'fanout', {
            durable: true
        });

        if (err)
            res.send(err);
        else
            amqpChannel.publish('cadastrar_sala', '', Buffer.from(JSON.stringify(sala)), options);

        res.json(sala);
    });
};

exports.listar_todas_salas = function(req, res) {
    Sala.find({}, function(err, sala) {
        if (err)
            res.send(err);
        res.json(sala);
    });
};

exports.entrar_em_sala = function(req, res) {

    const idSala = req.params.idSala;
    const idUser = req.params.idUser;

    Sala.findById(idSala, function(err, sala) {

        if (sala.participantes.includes(idUser)) {
            res.json({ sucesso: false, mensagem: 'Você já participa dessa sala.'})
        } else {
            sala.participantes.push(idUser);

            Sala.findOneAndUpdate({
                _id: idSala
            }, sala, { upsert: true }, function(err, resFinded) {
                amqpChannel.assertExchange('entrar_em_sala', 'fanout', {
                    durable: true
                });

                if (err)
                    res.send(err);
                else
                    amqpChannel.publish('entrar_em_sala', '', Buffer.from(JSON.stringify(sala)), options);

                res.json(sala);
            });
        }
    });
};

exports.alterar_componente_sala = function(req, res) {

    const idSala = req.params.idSala;
    const componentes = req.body.componentes;
    const nome = req.body.nome;

    Sala.findById(idSala, function(err, sala) {
        sala.nome = nome;
        sala.componentes = componentes;

        Sala.findOneAndUpdate({
            _id: idSala
        }, sala, { upsert: true }, function(err, resFinded) {

            if (err)
                res.send(err);

            amqpChannel.assertExchange(`alterar_componente_sala.${idSala}`, 'fanout', {
                durable: true
            });

            amqpChannel.publish(`alterar_componente_sala.${idSala}`, '', Buffer.from(JSON.stringify(sala)), options);
            res.json(sala);
        });
    });
};

exports.buscar_sala = function(req, res) {
    Sala.findById(req.params.idSala, function(err, sala) {
        if (err)
            res.send(err);
        res.json(sala);
    });
};

exports.sala_usuario = function(req, res) {

    const idUser = req.params.idUser;

    Sala.find({criador: idUser}, function(err, sala) {
        if (err)
            res.send(err);
        res.json(sala);
    });
};

exports.iniciar_apresentacao = function(req, res) {

    const idSala = req.params.idSala;

    Sala.findById(idSala, function(err, sala) {
        // const novaSala = JSON.parse(JSON.stringify(sala));
        sala.apresentando = true;

        Sala.findOneAndUpdate({
            _id: idSala
        }, sala, { upsert: true }, function(err, resFinded) {

            if (err)
                res.send(err);

            amqpChannel.assertExchange(`live.${idSala}`, 'fanout', {
                durable: false
            });

            res.json(resFinded);

        });
    });
};

exports.finalizar_apresentacao = function(req, res) {

    const idSala = req.params.idSala;
    amqpChannel.publish(`live.${idSala}`, '', Buffer.from(JSON.stringify({ encerrar: true})), options);

    Sala.findById(idSala, function(err, sala) {
        sala.apresentando = false;

        Sala.findOneAndUpdate({
            _id: idSala
        }, sala, { upsert: true }, function(err, resFinded) {

            if (err)
                res.send(err);

            amqpChannel.deleteExchange(`live.${idSala}`);

            res.json(resFinded);

        });
    });
};

exports.selecionar_componente_apresentacao = function(req, res) {

    const idSala = req.params.idSala;
    const indiceComponente = req.params.indiceComponente;
    amqpChannel.publish(`live.${idSala}`, '', Buffer.from(JSON.stringify({ indice: indiceComponente})), options);

    Sala.findById(idSala, function(err, sala) {
        sala.indiceComponente = indiceComponente;

        Sala.findOneAndUpdate({
            _id: idSala
        }, sala, { upsert: true }, function(err, resFinded) {

            if (err)
                res.send(err);

            res.json(resFinded);

        });
    });
};

exports.votar_componente = function(req, res) {

    const idSala = req.params.idSala;
    const indiceComponente = req.params.indiceComponente;
    const item = req.params.item;

    amqpChannel.publish(`live.${idSala}`, '', Buffer.from(JSON.stringify({ componente: indiceComponente, item: item})), options);

    Sala.findById(idSala, function(err, sala) {
        sala.componentes[indiceComponente].quiz.options.answers[item-1].votes++;

        Sala.findOneAndUpdate({
            _id: idSala
        }, sala, { upsert: true }, function(err, resFinded) {

            if (err)
                res.send(err);

            res.json(resFinded);

        });
    });
};

