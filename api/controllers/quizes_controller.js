'use strict';

const mongoose = require('mongoose');
const Task = mongoose.model('Tasks');
var amqp = require('amqplib/callback_api');

let amqpChannel = null;

amqp.connect('amqp://34.82.122.214:5672', function(error0, connection) {
    console.log('[#] AMQP connected!');
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        console.log('[#] AMPQ channel created!');
        amqpChannel = channel;
    });
});

const options = {
    persistent: true,
    noAck: false,
    timestamp: Date.now(),
    contentEncoding: "utf-8",
    contentType: "text/plain"
};

exports.list_all_tasks = function(req, res) {
    Task.find({}, function(err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.create_a_task = function(req, res) {
    const new_task = new Task(req.body);

    new_task.save(function(err, task) {

        amqpChannel.assertExchange('create_a_task', 'fanout', {
            durable: true
        });
        amqpChannel.publish('create_a_task', '', Buffer.from(JSON.stringify(task)), options);

        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.read_a_task = function(req, res) {
    Task.findById(req.params.taskId, function(err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.update_a_task = function(req, res) {
    Task.findOneAndUpdate({_id: req.params.taskId}, req.body, {new: true, useFindAndModify: false}, function(err, task) {

        amqpChannel.assertExchange('update_a_task', 'fanout', {
            durable: true
        });
        amqpChannel.publish('create_a_task', '', Buffer.from(JSON.stringify(task)), options);

        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.delete_a_task = function(req, res) {


    Task.remove({
        _id: req.params.taskId
    }, function(err, task) {

        amqpChannel.assertExchange('delete_a_task', 'fanout', {
            durable: true
        });
        amqpChannel.publish('create_a_task', '', Buffer.from(JSON.stringify(task)), options);

        if (err)
            res.send(err);

        res.json({ message: 'Task successfully deleted' });
    });
};
