'use strict';

const mongoose = require('mongoose');
const Usuario = mongoose.model('Usuario');
var amqp = require('amqplib/callback_api');

let amqpChannel = null;

amqp.connect('amqp://34.82.122.214:5672', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        amqpChannel = channel;
    });
});
const options = {
    persistent: true,
    noAck: false,
    timestamp: Date.now(),
    contentEncoding: "utf-8",
    contentType: "text/plain"
};

exports.cadastrar_usuario = function(req, res) {
    const novo_usuario = new Usuario(req.body);

    novo_usuario.save(function(err, usuario) {
        req.session.idUsuario = usuario._id;

        amqpChannel.assertExchange('cadastrar_usuario', 'fanout', {
            durable: true
        });

        if (err)
            res.send(err);
        else
            amqpChannel.publish('cadastrar_usuario', '', Buffer.from(JSON.stringify(usuario)), options);
        res.json(usuario);
    });
};

exports.verificar_usuario_session = function(req, res) {
    const idUsuario = req.session.idUsuario;
    let response = {
        ativo: false,
        usuario: {}
    };

    if (idUsuario) {
        response.ativo = true;
        Usuario.findById(idUsuario, function(err, usuario) {
            response.usuario = usuario;
            res.json(response);
        });
    } else {
        res.json(response);
    }


};

exports.verificar_usuario = function(req, res) {
    const idUser = req.params.idUser;

    let response = {
        sucesso: false,
    };

    if (idUser) {
        response.sucesso = true;
        Usuario.findById(idUser, function(err, usuario) {
            response.usuario = usuario;
            res.json(response);
        });
    } else {
        res.json(response);
    }


};
