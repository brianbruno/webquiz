'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*const Usuario = require('./usuario_model'); //created model loading here

const UsuarioSchema = mongoose.model('Usuario').schema;*/

const SalaSchema = new Schema({
    nome: {
        type: String,
        trim: true,
        required: 'É preciso digitar o nome dala'
    },
    criador: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario',
        required: 'É preciso informar o criador da sala'
    },
    data_criacao: {
        type: Date,
        default: Date.now
    },
    ativo: {
        type: Boolean,
        default: true
    },
    apresentando: {
        type: Boolean,
        default: false,
    },
    participantes: {
        type: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Usuario'
            }
        ]
    },
    componentes: {
        type: [
            {
                type: mongoose.Schema.Types.Object
            }
        ]
    },
    indiceComponente: {
        type: Number,
        default: 0,
    },

});

module.exports = mongoose.model('Sala', SalaSchema);
/*

{
    titulo: {
        type: String
    },
    subtitulo: {
        type: String
    },
    tipo: {
        type: String
    },
    configuracoes: {
        exibirlikes: {
            type: Boolean
        },
        exibirpessoas: {
            type: Boolean
        },
        exibirlink: {
            type: Boolean
        }
    },
    texto: {
        type: String
    },
    quiz: {
        type: {
            options: {
                type: mongoose.Schema.Types.Object
            }
        }
    }
}
*/
