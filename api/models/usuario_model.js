'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UsuarioSchema = new Schema({
    nome: {
        type: String,
        trim: true,
        required: 'É preciso digitar seu nome.'
    },
    data_criacao: {
        type: Date,
        default: Date.now
    },
    ativo: {
        type: Boolean,
        default: true
    },
});

module.exports = mongoose.model('Usuario', UsuarioSchema);
