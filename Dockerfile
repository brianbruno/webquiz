FROM node:10

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
RUN apt install git
RUN git clone https://gitlab.com/brianbruno/webquiz .

RUN npm install

EXPOSE 80
CMD [ "node", "index.js" ]
