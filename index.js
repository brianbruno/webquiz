const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose') ;
const cors = require('cors');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const Usuario = require('./api/models/usuario_model');
const Sala = require('./api/models/sala_model');

const app = express();
app.use(cors({origin: '*'}));
// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const port = process.env.PORT || 80;

// mongoose instance connection url connection
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://root:Mongo!DB!MainConfiguration@34.82.122.214:27017/dev_realtimedb?authSource=admin&w=1', {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    auth: { authdb: "admin" }
});

app.use(session({
    secret: 'key_secret',
    saveUninitialized: true,
    resave: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const routes_usuario = require('./api/routes/usuario_routes');
routes_usuario(app);

const routes_sala = require('./api/routes/sala_routes');
routes_sala(app);

app.listen(port);

console.log('[#] Backend started on port: ' + port);
